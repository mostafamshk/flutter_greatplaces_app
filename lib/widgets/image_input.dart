// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspath;

class ImageInputWidget extends StatefulWidget {
  Function onImageSelected;

  ImageInputWidget({
    Key? key,
    required this.onImageSelected,
  }) : super(key: key);

  @override
  State<ImageInputWidget> createState() => _ImageInputWidgetState();
}

class _ImageInputWidgetState extends State<ImageInputWidget> {
  File? _imageFile;

  Future<void> _takePicture() async {
    final imagePicker = ImagePicker();
    try {
      final chosenImage =
          await imagePicker.pickImage(source: ImageSource.camera);
      setState(() {
        _imageFile = File(chosenImage!.path);
      });
      final appDir = await syspath.getApplicationDocumentsDirectory();
      final fileName = path.basename(chosenImage!.path);
      await chosenImage.saveTo("${appDir.path}/$fileName");
      final finalImage = File("${appDir.path}/$fileName");

      widget.onImageSelected(finalImage);
    } catch (exception) {
      print("exception caught: ${exception.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 150,
          height: 100,
          decoration: BoxDecoration(
              border: Border.all(
                  width: 1, color: Theme.of(context).colorScheme.primary)),
          child: _imageFile != null
              ? Image.file(_imageFile!)
              : Center(
                  child: Text(
                    "No picture taken.",
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.primary),
                  ),
                ),
        ),
        Expanded(
            child: TextButton.icon(
                onPressed: () {
                  _takePicture();
                },
                icon: Icon(Icons.camera),
                label: Text("Take Picture")))
      ],
    );
  }
}
