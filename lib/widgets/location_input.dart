// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:location/location.dart';

import 'package:greatplaces/helpers/location_helper.dart';
import 'package:greatplaces/screens/map_screen.dart';

class LocationInputWidget extends StatefulWidget {
  final Function onSelectLocation;

  const LocationInputWidget({
    Key? key,
    required this.onSelectLocation,
  }) : super(key: key);

  @override
  State<LocationInputWidget> createState() => _LocationInputWidgetState();
}

class _LocationInputWidgetState extends State<LocationInputWidget> {
  String? _locationPreviewImageUrl;

  Future<void> _onChooseLocationOnTheMapSelected() async {
    final selectedLocation = await Navigator.of(context).push(MaterialPageRoute(
      fullscreenDialog: true,
      builder: (context) => MapScreen(
        isSelecting: true,
      ),
    ));
    print("Selected location: $selectedLocation");
    if (selectedLocation == null) {
      return;
    }
  }

  Future<void> _getUserCurrentLocation() async {
    try {
      final locationData = await Location().getLocation();
      print("user latitude: ${locationData.latitude}");
      print("user longitude: ${locationData.longitude}");
      setState(() {
        _locationPreviewImageUrl = LocationHelper.generateLocationPreviewImage(
            lat: locationData.latitude!, lon: locationData.longitude!);
      });
      await widget.onSelectLocation(
          locationData.latitude, locationData.longitude);
    } catch (exception) {
      print("exception in _getUserCurrentLocation: ${exception.toString()}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 170,
          decoration: BoxDecoration(
              border: Border.all(
                  width: 1, color: Theme.of(context).colorScheme.primary)),
          child: _locationPreviewImageUrl == null
              ? Center(
                  child: Text(
                    "No location chosen!",
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.primary,
                        fontWeight: FontWeight.bold),
                  ),
                )
              : Image.network(
                  _locationPreviewImageUrl!,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton.icon(
                onPressed: _getUserCurrentLocation,
                icon: Icon(Icons.location_on),
                label: Text("Current location"),
                style: TextButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.primary)),
            TextButton.icon(
                onPressed: null,
                icon: Icon(Icons.map),
                label: Text("Select on map"),
                style: TextButton.styleFrom(
                    foregroundColor: Theme.of(context).colorScheme.primary))
          ],
        ),
      ],
    );
  }
}
