import 'dart:convert';

import 'package:http/http.dart' as http;

const NESHAN_MAP_KEY = "service.4c9493907dbd415c92a11094d675e78f";

class LocationHelper {
  static String generateLocationPreviewImage(
      {required double lat, required double lon}) {
    return "https://api.neshan.org/v2/static?key=$NESHAN_MAP_KEY&type=standard-day&zoom=17&center=$lat,$lon&width=800&height=600&marker=red";
  }

  static Future<String> getLocationAddress(
      {required double lat, required double lon}) async {
    final String url = 'https://api.neshan.org/v5/reverse?lat=$lat&lng=$lon';

    final response =
        await http.get(Uri.parse(url), headers: {'Api-Key': NESHAN_MAP_KEY});
    final address = json.decode(response.body)['formatted_address'];

    return address;
  }
}
