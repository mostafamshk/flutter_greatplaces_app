import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:greatplaces/widgets/image_input.dart';
import 'package:greatplaces/widgets/location_input.dart';
import 'package:provider/provider.dart';

import '../helpers/location_helper.dart';
import '../models/place.dart';
import '../providers/places.dart';

class AddGreatPlaceScreen extends StatefulWidget {
  static const routeName = '/add-great-place-screen';

  const AddGreatPlaceScreen({super.key});

  @override
  State<AddGreatPlaceScreen> createState() => _AddGreatPlaceScreenState();
}

class _AddGreatPlaceScreenState extends State<AddGreatPlaceScreen> {
  final _titleController = TextEditingController();
  File? _imageFile;
  PlaceLocation? _selectedLocation;

  void _onImageSelected(File selectedImage) {
    _imageFile = selectedImage;
  }

  Future<void> _onAddPlace() async {
    if (_imageFile == null ||
        _titleController.text.isEmpty ||
        _selectedLocation == null) {
      return;
    }
    await Provider.of<Places>(context, listen: false).addPlace(
        _titleController.text,
        _imageFile!,
        _selectedLocation!.latitude,
        _selectedLocation!.longitude,
        _selectedLocation!.address as String);
    Navigator.of(context).pop();
  }

  Future<void> _onSelectLocation(double lat, double lon) async {
    final address = await LocationHelper.getLocationAddress(lat: lat, lon: lon);
    setState(() {
      _selectedLocation =
          PlaceLocation(latitude: lat, longitude: lon, address: address);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add a great place"),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    TextField(
                      decoration: InputDecoration(labelText: 'Title'),
                      controller: _titleController,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ImageInputWidget(onImageSelected: _onImageSelected),
                    SizedBox(
                      height: 20,
                    ),
                    LocationInputWidget(
                      onSelectLocation: _onSelectLocation,
                    )
                  ],
                ),
              ),
            )),
            ElevatedButton.icon(
              onPressed: () {
                _onAddPlace();
              },
              icon: Icon(Icons.add),
              label: Text("Add Place"),
              style: ElevatedButton.styleFrom(
                elevation: 0,
                padding: EdgeInsets.symmetric(vertical: 20),
                backgroundColor: Theme.of(context).colorScheme.secondary,
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
            )
          ],
        ),
      ),
    );
  }
}
