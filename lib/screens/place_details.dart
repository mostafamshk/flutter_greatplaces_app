import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:provider/provider.dart';

import '../providers/places.dart';

class GreatPlaceDetails extends StatelessWidget {
  static const routeName = '/great-place-details-screen';

  const GreatPlaceDetails({super.key});

  @override
  Widget build(BuildContext context) {
    final placeId = ModalRoute.of(context)!.settings.arguments! as String;
    final place = Provider.of<Places>(context).findById(placeId);
    return Scaffold(
      appBar: AppBar(
        title: Text(place.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            height: 350,
            child: Image.file(
              place.image,
              fit: BoxFit.cover,
              width: double.infinity,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(place.location!.address!),
          SizedBox(
            height: 20,
          ),
          TextButton(onPressed: null, child: Text("Preview on map"))
        ],
      ),
    );
  }
}
