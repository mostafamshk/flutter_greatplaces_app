// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import '../models/place.dart';

class MapScreen extends StatefulWidget {
  final PlaceLocation initLocation;
  final bool isSelecting;
  const MapScreen({
    Key? key,
    this.initLocation = const PlaceLocation(latitude: 35.69, longitude: 51.49),
    this.isSelecting = false,
  }) : super(key: key);

  @override
  State<MapScreen> createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  PlaceLocation? _selectedLocation;

  // @override
  // void initState() {
  //   Future.delayed(Duration.zero).then((_) {
  //     selectedLocation = widget.initLocation;
  //   });
  //   super.initState();
  // }

  void _onSelectedLocationConfirmed() {
    Navigator.of(context).pop({
      'isSelecting': widget.isSelecting,
      'lat': _selectedLocation!.latitude,
      'lon': _selectedLocation!.longitude,
      'address': _selectedLocation!.address
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Select on map"),
        actions: [
          if (widget.isSelecting)
            TextButton(
                onPressed: _selectedLocation == null
                    ? null
                    : () {
                        _onSelectedLocationConfirmed();
                      },
                child: Text(
                  "Select",
                  style: TextStyle(color: Colors.white),
                ))
        ],
      ),
      body: Container(
          width: double.infinity,
          child: true
              ? Center(
                  child: Text(
                    "Select on map screen!",
                  ),
                )
              : Container()
          // GoogleMap(
          //     initialCameraPosition: CameraPosition(
          //         target: LatLng(widget.initLocation.latitude,
          //             widget.initLocation.longitude),
          //         zoom: 16),
          //     markers: {
          //       Marker(
          //           markerId: MarkerId("m1"),
          //           position: LatLng(_selectedLocation!.latitude,
          //               _selectedLocation!.longitude))
          //     },
          //     mapType: MapType.normal,
          //     onTap: (argument) {
          //       setState(() {
          //         _selectedLocation = PlaceLocation(
          //             latitude: argument.latitude,
          //             longitude: argument.longitude);
          //       });
          //     },
          //   ),
          ),
    );
  }
}
