import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:greatplaces/screens/add_place_screen.dart';
import 'package:greatplaces/screens/place_details.dart';
import 'package:provider/provider.dart';

import '../providers/places.dart';

class GreatPlacesListScreen extends StatelessWidget {
  static const routeName = '/great-places-list-screen';

  const GreatPlacesListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Great Places List"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).pushNamed(AddGreatPlaceScreen.routeName);
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: Container(
        width: double.infinity,
        child: FutureBuilder(
          future:
              Provider.of<Places>(context, listen: false).fetchAndSetPlaces(),
          builder: (context, snapshot) => snapshot.connectionState ==
                  ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Consumer<Places>(
                  child: Center(
                    child: Text("No such place is available. add one..."),
                  ),
                  builder: (context, places, child) => places.items.isEmpty
                      ? child!
                      : ListView.builder(
                          itemCount: places.items.length,
                          itemBuilder: (context, index) => ListTile(
                            title: Text(places.items[index].title),
                            subtitle:
                                Text(places.items[index].location!.address!),
                            leading: CircleAvatar(
                                backgroundImage:
                                    FileImage(places.items[index].image)),
                            onTap: () {
                              Navigator.of(context).pushNamed(
                                  GreatPlaceDetails.routeName,
                                  arguments: places.items[index].id);
                            },
                          ),
                        ),
                ),
        ),
      ),
    );
  }
}
