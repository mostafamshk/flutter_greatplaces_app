import 'dart:io';

import 'package:flutter/material.dart';
import 'package:greatplaces/helpers/db_helper.dart';
import 'package:greatplaces/helpers/location_helper.dart';

import '../models/place.dart';

class Places with ChangeNotifier {
  List<Place> _items = [];

  List<Place> get items {
    return [..._items];
  }

  Future<void> addPlace(
      String title, File image, double lat, double lon, String address) async {
    final newPlace = Place(
        id: DateTime.now().toString(),
        title: title,
        image: image,
        location:
            PlaceLocation(latitude: lat, longitude: lon, address: address));

    _items.add(newPlace);
    notifyListeners();
    await DBHelper.insert('user_places', {
      'id': newPlace.id,
      'title': newPlace.title,
      'image': newPlace.image.path,
      'lat': newPlace.location!.latitude,
      'lon': newPlace.location!.longitude,
      'address': newPlace.location!.address
    });
  }

  Future<void> fetchAndSetPlaces() async {
    try {
      final placesListMap = await DBHelper.getPlaces("user_places");
      print(placesListMap);
      _items = placesListMap
          .map((e) => Place(
              id: e['id'],
              title: e['title'],
              image: File(e['image']),
              location: PlaceLocation(
                  latitude: double.parse(e['lat']),
                  longitude: double.parse(e['lon']),
                  address: e['address'])))
          .toList();

      print("fetched Items: ${_items}");
      notifyListeners();
    } catch (exception) {
      print(exception.toString());
    }
  }

  Place findById(String id) {
    return _items.firstWhere((element) => id == element.id);
  }
}
