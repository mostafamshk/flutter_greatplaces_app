// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

class PlaceLocation {
  final double latitude;
  final double longitude;
  final String? address;
  const PlaceLocation({
    required this.latitude,
    required this.longitude,
    this.address = '',
  });
}

class Place {
  String id;
  String title;
  File image;
  PlaceLocation? location;
  Place({
    required this.id,
    required this.title,
    required this.image,
    required this.location,
  });
}
