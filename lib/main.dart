import 'package:flutter/material.dart';
import 'package:greatplaces/screens/add_place_screen.dart';
import 'package:greatplaces/screens/place_details.dart';
import 'package:greatplaces/screens/places_list_screen.dart';
import 'package:provider/provider.dart';

import 'providers/places.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Places places = Places();

  MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    
     return ChangeNotifierProvider.value(
      value: places,
       child: MaterialApp(
          title: 'Great Places',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.indigo , secondary: Colors.amber)
          ),
          home: GreatPlacesListScreen(),
          routes: {
            GreatPlacesListScreen.routeName:(ctx) => GreatPlacesListScreen() ,
            GreatPlaceDetails.routeName:(ctx) => GreatPlaceDetails(),
            AddGreatPlaceScreen.routeName: (ctx) => AddGreatPlaceScreen() 
          },
         ),
     );
  }
}

